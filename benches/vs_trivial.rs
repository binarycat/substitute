#![feature(test)]

extern crate test;

use std::hint::black_box;
use std::collections::HashMap;
use test::Bencher;
use substitute::substitute;

static TEMPL1: &str = "Hello %name%, I hope you have a %quality% %weekday%.";

#[bench]
fn sub_hashmap(b: &mut Bencher) {
	b.iter(|| {
		let m: HashMap<&str, &str> = [
		("name", "Alice"),
		("quality", "good"),
		("weekday", "Thursday"),
		].into();
		substitute(TEMPL1, black_box(&m))
	});
}

#[bench]
fn sub_slicetuple(b: &mut Bencher) {
	b.iter(|| {
		let m: &[(&str, &str)] = &[
			("name", "Alice"),
			("quality", "good"),
			("weekday", "Thursday"),
		];
		substitute(TEMPL1, black_box(m))
	});
}

#[bench]
fn trivial(b: &mut Bencher) {
	b.iter(|| TEMPL1.replace("%name%", "Alice")
		   .replace("%quality%", "good")
		   .replace("%weekday%", "Thursday"));
		   
}

#[bench]
fn stdfmt(b: &mut Bencher) {
	b.iter(|| {
		let name = black_box("Alice");
		let quality = black_box("good");
		let weekday = black_box("Thursday");
		black_box(format!("Hello {name}, I hope you have a {quality} {weekday})"));
	});
}
