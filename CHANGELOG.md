# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.2.0 - 2024-11-07

### Fixed
* `%%` in templates is now properly replaced with `%`

### Changed
* `Error`, `ErrorKind`, and `Substitute` now have an additional type paramater (with a default value)
* `Error::kind` now returns a reference.

### Depricated
* `substitute` (use `substitute_to_string` instead)

### Added
* `no_std` support
* support for non-utf8 templates
* `Output` trait to facilitate the above features
* `substitute_to` function as the new generic interface

### Removed
* `ErrorKind` is no longer `Copy`

## 0.1.0 - 2024-11-05
Inital Release
